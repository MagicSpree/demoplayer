package com.example.javafx.observer;

import java.util.ArrayList;
import java.util.List;

public class LiveData<T> {

    private final List<Observer<T>> typeList = new ArrayList<>();

    public void observe(Observer<T> observer) {
        typeList.add(observer);
    }

    public void addValue(T type){
        notifyObservers(type);
    }

    public void notifyObservers(T type) {
        for (Observer<T> o : typeList)
            o.onChange(type);
    }
}
