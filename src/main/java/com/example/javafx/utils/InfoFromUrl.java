package com.example.javafx.utils;

public class InfoFromUrl {

    public static String getNameOfFile(String url){
        return url.substring(url.lastIndexOf("/"),url.lastIndexOf(".")).replaceAll("/","");
    }

    public static String getTypeOfFile(String url){
        return url.substring(url.lastIndexOf(".")).replaceAll("\\.","");
    }
}
