package com.example.javafx.observer;

import java.io.File;

public interface Observer<T> {
    void onChange(T type);
}
