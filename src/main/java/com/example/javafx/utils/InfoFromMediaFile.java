package com.example.javafx.utils;


import com.mpatric.mp3agic.Mp3File;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class InfoFromMediaFile {

    private Mp3File mp3File;

    public InfoFromMediaFile(Mp3File mp3File){
        this.mp3File = mp3File;
    }

    public String getAuthorName() throws UnsupportedEncodingException {
        if (mp3File.hasId3v1Tag()){
            return new String(mp3File.getId3v1Tag().getArtist().getBytes(StandardCharsets.ISO_8859_1),"windows-1251");
        }else if (mp3File.hasId3v2Tag()){
            return new String(mp3File.getId3v1Tag().getArtist().getBytes(StandardCharsets.ISO_8859_1),"windows-1251");
        }else {
            return "Автор неизвестен";
        }
    }

    public String getSongName() throws UnsupportedEncodingException {
        if (mp3File.hasId3v1Tag()){
            return new String(mp3File.getId3v1Tag().getTitle().getBytes(StandardCharsets.ISO_8859_1),"windows-1251");
        }else if (mp3File.hasId3v2Tag()){
            return new String(mp3File.getId3v1Tag().getTitle().getBytes(StandardCharsets.ISO_8859_1),"windows-1251");
        }else {
            return "Название неизвестно";
        }
    }
}
