package com.example.javafx.network;

public class Response<T> {
    private T success = null;
    private String error;
    private boolean isError = false;

    public boolean isError() {
        return isError;
    }

    public T getSuccess() {
        return success;
    }

    public void setSuccess(T success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        isError = true;
        this.error = error;
    }
}
