package com.example.javafx.controller;

import com.example.javafx.network.Response;
import com.example.javafx.utils.InfoFromMediaFile;
import com.example.javafx.network.DownloaderFile;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class PlayerController {
    @FXML
    public Label nameMusic;
    @FXML
    private Slider sliderMusic;
    @FXML
    private ImageView imageMusic;
    @FXML
    private Label beginSec;
    @FXML
    private Label endSec;

    MediaPlayer mediaPlayer;

    @FXML
    protected void onPlayButtonClick() {
        String urlMusic = "https://ru.drivemusic.me/dl/8BpXXuLrpfBpEwye3-SJBA/1639722057/download_music/2018/10/gaullin-moonlight.mp3";
        String pathMusic = "/tmp/music/morgen.mp3";
        DownloaderFile downloader = new DownloaderFile();
        downloader.liveData.observe(this::playMusic);
        downloader.download(urlMusic, pathMusic);
    }

    @FXML
    protected void onPauseButtonClick() {
        if (mediaPlayer != null) {
            if (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING)
                mediaPlayer.pause();
            else {
                mediaPlayer.play();
            }
        }
    }

    @FXML
    protected void onReturnButtonClick() {
        mediaPlayer.play();
    }

    private void updatesValues() {
        Platform.runLater(() -> sliderMusic.setValue(mediaPlayer.getCurrentTime().toMillis() /
                mediaPlayer.getTotalDuration().toMillis() * 100));

        beginSec.setText(millisecondsToTime((int) mediaPlayer.getCurrentTime().toMillis()));
    }

    @FXML
    private void downloadImage() {
        String urlPicture = "https://avatars.mds.yandex.net/i?id=1613a86b79c57ca8dde2605a32821568-5220205-images-thumbs&n=13&exp=1";
        String pathPicture = "/tmp/music/sss.jpg";
        DownloaderFile downloader = new DownloaderFile();
//        downloader.liveData.observe(this::setImageMusic);
        downloader.download(urlPicture, pathPicture);
    }

    private void playMusic(Response<File> musicFile) {

        if (!musicFile.isError()) {

            Media sound = new Media(musicFile.getSuccess().toURI().toString());
            mediaPlayer = new MediaPlayer(sound);

            mediaPlayer.setOnReady(() -> {
                endSec.setText(millisecondsToTime((int) sound.getDuration().toMillis()));
                sliderMusic.setDisable(false);
                setInfoMusic(musicFile.getSuccess());
                mediaPlayer.play();
            });

            mediaPlayer.currentTimeProperty().addListener(ov -> updatesValues());
            sliderMusic.valueProperty().addListener(ov -> {
                if (sliderMusic.isPressed()) {
                    mediaPlayer.seek(mediaPlayer.getMedia().getDuration().multiply(sliderMusic.getValue() / 100));
                }
            });
        }
        else
            System.out.println(musicFile.getError());
    }

    private void setImageMusic(File musicFile) {
        Image image = new Image(musicFile.toURI().toString(), 300, 300, false, false);
        imageMusic.setImage(image);
    }

    private String millisecondsToTime(int millis) {
        int min = (int) TimeUnit.MILLISECONDS.toMinutes(millis);
        int sec = (int) (TimeUnit.MILLISECONDS.toSeconds(millis) % 60);
        return String.format("%02d:%02d", min, sec);
    }

    private void setInfoMusic(File music) {
        try {
            InfoFromMediaFile info = new InfoFromMediaFile(new Mp3File(music.getPath()));
            nameMusic.setText(info.getAuthorName() + " - " + info.getSongName());
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
        }
    }

//    private File isCheckResponse(Response<File> fileResponse){
//        if (fileResponse.isError())
//            System.out.println(fileResponse.getError());
//        else
//            return fileResponse.getSuccess();
//    }
}